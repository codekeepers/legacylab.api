package calculate

import (
	"bufio"
	"os"
	"strings"
)

var (
	readFileFunc = readFile
)

// Complexity calculates the indentation-based calculate of the given file by
// counting the leading spaces of each line. The sum of all spaces is set in
// relation to the number of lines what gives the overall calculate of the
// file.
func Complexity(path string) (int, float32, bool) {
	lines, found := readFileFunc(path)
	if found {
		lines = replaceTabs(lines)
		lines = removeComments(lines)
		lines = removeEmptyLines(lines)
		var indent int
		for _, l := range lines {
			indent = indent + leadingSpaces(l)
		}
		return len(lines), float32(indent) / float32(len(lines)), true
	}
	return 0, 0, false
}

func readFile(path string) ([]string, bool) {
	file, err := os.Open(path)
	if err != nil {
		panic(err)
		return nil, false
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	var lines []string
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, true
}

func leadingSpaces(line string) int {
	return len(line) - len(strings.TrimLeft(line, " "))
}

func replaceTabs(lines []string) []string {
	var result []string
	for _, l := range lines {
		l = strings.ReplaceAll(l, "\t", "    ")
		result = append(result, l)
	}
	return result
}

func removeComments(lines []string) []string {
	var result []string
	for _, l := range lines {
		trimmed := strings.Trim(l, " ")
		if !strings.HasPrefix(trimmed, "/*") && !strings.HasPrefix(trimmed, "*") && !strings.HasPrefix(trimmed, "//") {
			result = append(result, l)
		}
	}
	return result
}

func removeEmptyLines(lines []string) []string {
	var result []string
	for _, l := range lines {
		if len(strings.Trim(l, " ")) > 0 {
			result = append(result, l)
		}
	}
	return result
}
