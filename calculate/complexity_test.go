package calculate

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCalculateComplexity(t *testing.T) {
	tests := []struct {
		lines      []string
		length     int
		complexity float32
	}{
		{
			[]string{"func foo {", "println(i)", "}"}, 3, 0,
		},
		{
			[]string{"func foo {", "  println(i)", "}"}, 3, 0.6666667,
		},
		{
			[]string{
				"func foo {",
				"    // This is a comment",
				"    for _, tc := range tests {",
				"        println()",
				"    }",
				"",
				"}",
			},
			5,
			3.2,
		},
	}

	for _, tc := range tests {
		readFileFunc = func(path string) ([]string, bool) {
			return tc.lines, true
		}
		length, complexity, _ := Complexity("/")
		assert.Equal(t, tc.length, length)
		assert.Equal(t, tc.complexity, complexity)
	}
}

func TestLeadingSpaces(t *testing.T) {
	assert.Equal(t, 0, leadingSpaces("func"))
	assert.Equal(t, 2, leadingSpaces("  func"))
	assert.Equal(t, 4, leadingSpaces("    func"))
}
