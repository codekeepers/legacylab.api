build:
	env GOOS=linux CGO_ENABLED=0 go build -a -installsuffix cgo -o bin/legacylab.api http/main.go

deploy: build
	ssh root@95.217.180.178 "pkill legacylab.api"
	scp bin/legacylab.api root@95.217.180.178:~/
	ssh root@95.217.180.178 "sh -c 'nohup /root/legacylab.api > /dev/null 2>&1 &'"

.PHONY: build