package hotspots

type Repository interface {
	Checkout(url string) error
}
