package hotspots

import (
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var (
	isDirectoryExistsFunc = isDirectoryExists
	runFunc               = run
)

var SupportedGitRepositories = []string{"github", "gitlab"}

type Git struct {
	URL string
}

func (g Git) Checkout() error {
	err := isValidGitUrl(g.URL)
	if err != nil {
		return err
	}

	path := g.LocalPath()
	if isDirectoryExistsFunc(path) {
		log.WithFields(
			log.Fields{
				"url": g.URL,
				"dir": path,
			},
		).Info("Pulling repository")
		b, err := runFunc("git", "-C", path, "pull")
		if err != nil {
			return errors.New("git pull failed")
		}
		log.Infof("Git: %s", b)
	} else {
		log.WithFields(
			log.Fields{
				"url": g.URL,
				"dir": path,
			},
		).Info("Cloning repository")
		b, err := runFunc("git", "clone", g.URL, path)
		if err != nil {
			return err
		}
		log.Infof("Git: %s", b)
	}

	return nil
}

func (g Git) LocalPath() string {
	pathWithoutRepoBase := regexp.MustCompile(`https://(github|gitlab).com/`).ReplaceAllString(g.URL, "")
	pathWithoutGitSuffix := regexp.MustCompile(`.git`).ReplaceAllString(pathWithoutRepoBase, "")
	return fmt.Sprintf("/tmp/%s", pathWithoutGitSuffix)
}

func run(name string, arg ...string) ([]byte, error) {
	log.WithFields(
		log.Fields{
			"cmd":  name,
			"args": arg,
		},
	).Info("Running command")
	out, err := exec.Command(name, arg...).Output()
	if err != nil {
		return nil, err
	}
	return out, nil
}

func isValidGitUrl(url string) error {
	if strings.TrimSpace(url) == "" {
		return errors.New("empty repository URL")
	}

	if !strings.Contains(url, "https://") || !strings.HasSuffix(url, ".git") {
		return errors.New("invalid repository URL")
	}

	if !isSupported(url) {
		return errors.New("unsupported repository")
	}
	return nil
}

func isSupported(repo string) bool {
	for _, v := range SupportedGitRepositories {
		if strings.Contains(repo, v) {
			return true
		}
	}
	return false
}

// ChangeFrequency calculates the number of changes applied to each file in the
// given local repository. The resulting list is sorted by number of changes in
// descending order. The result list is limited according to the given limit.
func (g Git) ChangeFrequency(limit int) ([]File, error) {
	path := g.LocalPath()
	os.Chdir(path)
	cmd := fmt.Sprintf("git log --format=format: --name-only | egrep -v '^$' | sort | uniq -c | sort -r | head -%d", limit)
	out, err := exec.Command("bash", "-c", cmd).Output()
	if err != nil {
		return nil, err
	}
	return toArray(path, string(out))
}

func (g Git) RemoveLocalRepository() error {
	return removeDirectory(g.LocalPath())
}

func toArray(path string, s string) ([]File, error) {
	a := strings.Split(s, "\n")
	var result []File
	var i = 0
	for _, f := range a {
		name := toName(f)
		if exists(path, name) && isLanguageFile(name) && !isTest(name) {
			file := NewFile(f)
			if ts, err := timestamp(file); err == nil {
				file.Age = ts
				result = append(result, file)
			}
			i++
			if i > 50 {
				break
			}
		}
	}
	return result, nil
}

func toName(s string) string {
	a := strings.Split(strings.Trim(s, " "), " ")
	if len(a) == 2 {
		return a[1]
	}
	return ""
}

func exists(path string, name string) bool {
	if _, err := os.Stat(fmt.Sprintf("%s/%s", path, name)); err == nil {
		return true
	}
	return false
}

func timestamp(file File) (time.Time, error) {
	b, err := runFunc("git", "--no-pager", "log", "-1", "--format=\"%at\"", file.Name)
	if err != nil {
		log.Error(err)
		return time.Now(), err
	}

	ts := strings.Trim(string(b), "\"\n")
	i, err := strconv.ParseInt(ts, 10, 64)
	if err != nil {
		log.Error(err)
		return time.Now(), err
	}
	tm := time.Unix(i, 0)
	return tm, nil
}

func isLanguageFile(s string) bool {
	return strings.HasSuffix(s, ".go") || strings.HasSuffix(s, ".java")
}

func isTest(s string) bool {
	return strings.HasSuffix(s, "Tests.java")
}
