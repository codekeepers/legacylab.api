package hotspots

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestLocalGithubPath(t *testing.T) {
	git := Git{URL: "https://github.com/spring-projects/spring-data-jpa.git"}
	assert.Equal(t, "/tmp/spring-projects/spring-data-jpa", git.LocalPath())
}

func TestLocalGitlabPath(t *testing.T) {
	git := Git{URL: "https://gitlab.com/codekeepers/legacylab.ui.git"}
	assert.Equal(t, "/tmp/codekeepers/legacylab.ui", git.LocalPath())
}

func TestIsValidGitUrl(t *testing.T) {
	assert.Nil(t, isValidGitUrl("https://gitlab.com/codekeepers/legacylab.ui.git"))
	assert.Nil(t, isValidGitUrl("https://github.com/spring-projects/spring-data-jpa.git"))

	assert.NotNil(t, isValidGitUrl("https://github.com/spring-projects/spring-data-jpa"))
	assert.NotNil(t, isValidGitUrl("http://github.com/spring-projects/spring-data-jpa"))
	assert.NotNil(t, isValidGitUrl("https://bitbucket.com/spring-projects/spring-data-jpa.git"))
	assert.NotNil(t, isValidGitUrl(""))
	assert.NotNil(t, isValidGitUrl("   "))
}

func TestCheckoutShouldRejectEmptyRepo(t *testing.T) {
	git := Git{URL: "   "}
	err := git.Checkout()
	assert.Equal(t, errors.New("empty repository URL"), err)
}

func TestCheckoutShouldRejectUnsupportedRepo(t *testing.T) {
	git := Git{URL: "https://bitbucket.com/codekeepers/legacylab.ui.git"}
	err := git.Checkout()
	assert.Equal(t, errors.New("unsupported repository"), err)
}

func TestCheckoutShouldRejectInvalidUrl(t *testing.T) {
	git := Git{URL: "htps://bitbucket.com/codekeepers/legacylab.ui.git"}
	err := git.Checkout()
	assert.Equal(t, errors.New("invalid repository URL"), err)

	git.URL = "https://bitbucket.com/codekeepers/legacylab.ui"
	err = git.Checkout()
	assert.Equal(t, errors.New("invalid repository URL"), err)
}

func TestCheckoutShouldCallPull(t *testing.T) {
	isDirectoryExistsFunc = func(name string) bool {
		return true
	}

	runCalled := false
	runFunc = func(name string, arg ...string) ([]byte, error) {
		runCalled = true
		assert.Equal(t, "git", name)
		assert.True(t, contains(arg, "pull"))
		return nil, nil
	}

	git := Git{URL: "https://gitlab.com/codekeepers/legacylab.ui.git"}
	err := git.Checkout()
	assert.Nil(t, err)
	assert.True(t, runCalled)
}

func TestCheckoutShouldCallClone(t *testing.T) {
	isDirectoryExistsFunc = func(name string) bool {
		return false
	}

	runCalled := false
	runFunc = func(name string, arg ...string) ([]byte, error) {
		runCalled = true
		assert.Equal(t, "git", name)
		assert.True(t, contains(arg, "clone"))
		return nil, nil
	}

	git := Git{"https://gitlab.com/codekeepers/legacylab.ui.git"}
	err := git.Checkout()
	assert.Nil(t, err)
	assert.True(t, runCalled)
}

func contains(arg []string, s string) bool {
	for _, v := range arg {
		if v == s {
			return true
		}
	}
	return false
}
