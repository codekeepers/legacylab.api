package hotspots

import (
	"os"
	"strconv"
	"strings"
	"time"
)

type File struct {
	Name       string
	Commits    int
	Age        time.Time
	Lines      int
	Complexity float32
}

func NewFile(s string) File {
	a := strings.Split(strings.Trim(s, " "), " ")
	commits, err := strconv.Atoi(a[0])
	if err != nil {
		panic(err)
	}
	return File{
		Name:    a[1],
		Commits: commits,
	}
}

func isDirectoryExists(name string) bool {
	if _, err := os.Stat(name); !os.IsNotExist(err) {
		return true
	}
	return false
}

func removeDirectory(path string) error {
	return os.RemoveAll(path)
}
