package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/codekeepers/legacylab.api/calculate"
	"gitlab.com/codekeepers/legacylab.api/hotspots"
	"os"
	"text/tabwriter"
	"time"
)

func main() {
	url := "https://gitlab.com/codekeepers/legacylab.api.git"
	git := hotspots.Git{URL: url}
	if err := git.Checkout(); err != nil && err.Error() == "git pull failed" {
		log.Info("forcing checkout by removing existing repository")
		if err := git.RemoveLocalRepository(); err != nil {
			panic(err)
		}

		if err := git.Checkout(); err != nil {
			panic(err)
		}
	}

	files, err := git.ChangeFrequency(2000)
	if err != nil {
		panic(err)
	}

	w := new(tabwriter.Writer)
	w.Init(os.Stdout, 0, 8, 0, '\t', 0)
	_, _ = fmt.Fprintf(w, "%s\t%s\t%s\t%s\t%s\n", "Commits", "File", "Lines", "Age", "Complexity")
	path := git.LocalPath()
	for _, f := range files {
		var found bool
		f.Lines, f.Complexity, found = calculate.Complexity(fmt.Sprintf("%s/%s", path, f.Name))
		if found {
			_, _ = fmt.Fprintf(w, "%d\t%s\t%d\t%s\t%.2f\n", f.Commits, f.Name, f.Lines, f.Age.Format(time.DateTime), f.Complexity)
		}
	}
	_ = w.Flush()
}
