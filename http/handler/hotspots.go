package handler

import (
	"encoding/json"
	"fmt"
	"gitlab.com/codekeepers/legacylab.api/calculate"
	"gitlab.com/codekeepers/legacylab.api/hotspots"
	"net/http"
	"strings"
)

func Hotspots(w http.ResponseWriter, request *http.Request) {
	enableCors(&w)
	url := "https://gitlab.com/codekeepers/legacylab.api.git"

	repo := struct {
		Repository string
	}{}
	err := json.NewDecoder(request.Body).Decode(&repo)
	if err != nil || len(strings.TrimSpace(url)) == 0 {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	git := hotspots.Git{URL: repo.Repository}
	git.Checkout()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	files, err := git.ChangeFrequency(200000)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	b, err := json.Marshal(addComplexity(git, files))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(b)
}

func addComplexity(git hotspots.Git, files []hotspots.File) []hotspots.File {
	var withComplexity []hotspots.File
	path := git.LocalPath()
	for _, f := range files {
		f.Lines, f.Complexity, _ = calculate.Complexity(fmt.Sprintf("%s/%s", path, f.Name))
		withComplexity = append(withComplexity, f)
	}
	return withComplexity
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}
