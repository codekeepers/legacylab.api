package main

import (
	"flag"
	"github.com/gorilla/mux"
	"gitlab.com/codekeepers/legacylab.api/http/handler"
	"net/http"
)

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/hotspots", handler.Hotspots).Methods("POST")

	tls := flag.Bool("tls", true, "use tls")
	flag.Parse()
	var err error
	if *tls {
		err = http.ListenAndServeTLS(
			":8082",
			"/etc/letsencrypt/live/legacylab.ralfwirdemann.de/fullchain.pem",
			"/etc/letsencrypt/live/legacylab.ralfwirdemann.de/privkey.pem", r)
	} else {
		err = http.ListenAndServe(":8082", r)
	}
	if err != nil {
		panic(err)
	}
}
